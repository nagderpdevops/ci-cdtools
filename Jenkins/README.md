# Jenkins 

Jenkins is an extensible, open source continuous integration server. It builds and tests your software continuously and monitors the execution and status of remote jobs, making it easier for team members and users to regularly obtain the latest stable code. When it comes to continuous delivery, Jenkins uses a feature called Jenkins pipeline.

## What is Jenkins pipeline?

A pipeline is a collection of jobs that brings the software from version control into the hands of the end users by using automation tools. It is a feature used to incorporate continuous delivery in our software development workflow.

They represent multiple Jenkins jobs as one whole workflow in the form of a pipeline.

What do these pipelines do? These pipelines are a collection of Jenkins jobs which trigger each other in a specified sequence.